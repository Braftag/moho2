<?php
session_start();
$nom = $_SESSION['nom'];
$prenom = $_SESSION['prenom'];
$date = $_SESSION['date'];
$job = $_SESSION['job'];
$mail = $_SESSION['mail'];
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Petit test">
        <meta name="author" content="Moi">
    
        <title>Beta test</title>
        <!-- Bootstrap core CSS -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./slick/slick.css">
        <link rel="stylesheet" type="text/css" href="./slick/slick-theme.css">
        <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
        <script src="./slick/slick.min.js" type="text/javascript" charset="utf-8"></script>
    </head>
    <body>

    <div class="w3-light-grey w3-padding-large w3-padding-32 w3-margin-top" id="contact">
    <p class="w3-center w3-xxxlarge"> Parametres</p>
    <form name="test" class="w3-margin-right-32"  action="paremetre.php" method="post">

    <div class="w3-section w3-large"> 
    <p> <?php echo($prenom . " " . $nom); ?> </p>
    </div>

    <div class="w3-section w3-large"> 
        <p> <?php echo("Date de naissence : "." ".$date); ?> </p>
    </div>

    <div class="w3-section w3-large"> 
        <p> <?php echo("Adresse mail :"." ".$mail); ?> </p>
    </div>

    <div class="w3-section w3-large"> 
        <p> <?php echo("Job :"." ".$job); ?> </p>
    </div>
    <div class="w3-section">
    <input type="checkbox" name="case" id="case" /> <label for="case">J'accepte de partager mon historique resources consultées</label>
</div>
    <div class="w3-section">
    <input type="checkbox" name="case" id="case" /> <label for="case">Annuaire pro </label>
</div>
    <div class="w3-section">
    <input type="checkbox" name="case" id="case" /> <label for="case">J'acepte d'être géolocalisée dans le Moho </label>
</div>

    <button type="submit" class="w3-button w3-blue">Valider </button>
    </form>
</div>

</body>
</html>