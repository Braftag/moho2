<?php 
if(empty($_SESSION)) {
    session_start(); 
    $nom = isset($_SESSION['nom']) ? $_SESSION['nom'] : "Visiteur";
    $prenom = isset($_SESSION['prenom']) ? $_SESSION['prenom'] : "";
}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Petit test">
        <meta name="author" content="Moi">
    
        <title>Beta test</title>
        <!-- Bootstrap core CSS -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> 
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css" integrity="sha384-SI27wrMjH3ZZ89r4o+fGIJtnzkAnFs3E4qz9DIYioCQ5l9Rd/7UAa8DHcaL8jkWt" crossorigin="anonymous">
    </head>
    <body>

     <!-- Le logo -->
        <div class="w3-left">
            <p>Logo</p>
        </div>

        <!-- Le dropdown tro bo -->
        <div class="w3-right">
        <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?php echo($prenom . " ". $nom); ?> </button>
        
        <?php 
        if ($nom != "Visiteur") {
            echo("<div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>");
            echo("<a class='dropdown-item' href='#'>Votre liste de livre</a>");
            echo("<a class='dropdown-item' href='#'>Vos contacts</a>");
            echo("<a class='dropdown-item' href='#'>Historique d'emprunts</a>");
            echo("<a class='dropdown-item' href='#'>Vos reservations</a>");
            echo("<div class='dropdown-divider'></div>");
            echo("<a class='dropdown-item' href='parametre.php'>Parametre du compte</a>");
            echo("<a class='dropdown-item' href='destroy.php'>Deconnexion</a>");
        } else {
            echo("<div class='dropdown-menu  dropdown-menu-right' aria-labelledby='navbarDropdown'>");
            echo("<a class='dropdown-item' href='inscription.php'>Inscription</a></li>");
            echo("<a class='dropdown-item' href='connexion.php'>Connexion</a></li>");
            echo ("</div>");
        } // fin condition
        ?>
        </div>
        </div>
        </div>
        <!-- fin dropdown -->


        <!-- Jolie bouton -->
    <div class="w3-display-middle w3-padding-large w3-wide w3-text w3-center w3-animate-opacity">
        <a href="calendrier/index.php" class="w3-button w3-blue">Evenement</a>
        <a href="livre.php" class="w3-button w3-blue">Livres</a>
        <a href="emprunt.php" class="w3-button w3-blue">Emprunts</a>
        <a href="communaute.php" class="w3-button w3-blue">Communauté</a>

    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/js/bootstrap.min.js" integrity="sha384-3qaqj0lc6sV/qpzrc1N5DC6i1VRn/HyX4qdPaiEFbn54VjQBEU341pvjz7Dv3n6P" crossorigin="anonymous"></script>
    </body>
</html>