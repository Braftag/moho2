<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Petit test">
        <meta name="author" content="Moi">
    
        <title>Beta test</title>
        <!-- Bootstrap core CSS -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./slick/slick.css">
        <link rel="stylesheet" type="text/css" href="./slick/slick-theme.css">
        <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
        <script src="./slick/slick.min.js" type="text/javascript" charset="utf-8"></script>
    </head>
    <body>
        <style>
        body {
            background-color: #141414;
            color : white;
            font-family: Helvetica;
        }

        #navBar {
            background-color: transparent;
        }

            .slider {
            width: 100%;
            margin: 100px auto;
        }

        .slick-slide {
        margin: 0px 20px;
        }

        .slick-slide img {
        width: 100%;
        }


        .slick-slide {
        transition: all ease-in-out .3s;
        opacity: .2;
        }

        .slick-active {
        opacity: .5;
        }

        .slick-current {
        opacity: 1;
        }
        </style>
<header>
<div id ="navBar" class="w3-bar w3-margin-left">
  <a href="#" class="w3-bar-item w3-button">Logo</a>
  <a href="#" class="w3-bar-item w3-button">A la une</a>
  <a href="#" class="w3-bar-item w3-button">Ajout recents</a>
  <a href="#" class="w3-bar-item w3-button">Ma liste</a>
</div> 
</header>
   


    <h2 class="w3-text-align w3-jumbo" style="margin: 100px auto;"> Ajouts recents :</h2>



    <section class="center slider">
    <div>
    <a href="#"><img src="http://placehold.it/350x300?text=1"></a>
    </div>
    <div>
    <a href="#"><img src="http://placehold.it/350x300?text=2"></a>
    </div>
    <div>
    <a href="#"><img src="http://placehold.it/350x300?text=3"></a>
    </div>
    <div>
    <a href="#"><img src="http://placehold.it/350x300?text=4"></a>
    </div>
    <div>
    <a href="main.php"><img src="http://placehold.it/350x300?text=5"></a>
    </div>
    <div>
    <a href="#"><img src="http://placehold.it/350x300?text=6"></a>
    </div>
    <div>
    <a href="#"><img src="http://placehold.it/350x300?text=7"></a>
    </div>
    <div>
    <a href="#"><img src="http://placehold.it/350x300?text=8"></a>
    </div>
    <div>
    <a href="#"><img src="http://placehold.it/350x300?text=9"></a>
    </div>
  </section>

  <h2 class="w3-text-align w3-jumbo" style="margin: 100px auto;"> Tendances actuelles :</h2>



<section class="center slider">
<div>
  <img src="http://placehold.it/350x300?text=1">
</div>
<div>
  <img src="http://placehold.it/350x300?text=2">
</div>
<div>
  <img src="http://placehold.it/350x300?text=3">
</div>
<div>
  <img src="http://placehold.it/350x300?text=4">
</div>
<div>
  <img src="http://placehold.it/350x300?text=5">
</div>
<div>
  <img src="http://placehold.it/350x300?text=6">
</div>
<div>
  <img src="http://placehold.it/350x300?text=7">
</div>
<div>
  <img src="http://placehold.it/350x300?text=8">
</div>
<div>
  <img src="http://placehold.it/350x300?text=9">
</div>
</section>


<h2 class="w3-text-align w3-jumbo" style="margin: 100px auto;"> Categories :</h2>



<section class="center slider">
<div>
  <img src="http://placehold.it/350x300?text=1">
</div>
<div>
  <img src="http://placehold.it/350x300?text=2">
</div>
<div>
  <img src="http://placehold.it/350x300?text=3">
</div>
<div>
  <img src="http://placehold.it/350x300?text=4">
</div>
<div>
  <img src="http://placehold.it/350x300?text=5">
</div>
<div>
  <img src="http://placehold.it/350x300?text=6">
</div>
<div>
  <img src="http://placehold.it/350x300?text=7">
</div>
<div>
  <img src="http://placehold.it/350x300?text=8">
</div>
<div>
  <img src="http://placehold.it/350x300?text=9">
</div>
</section>


<script>
     $(document).on('ready', function() {
        $(".center").slick({
        dots: true,
        infinite: true,
        centerMode: true,
        slidesToShow: 5,
        slidesToScroll: 3
      });});
</script>
</head>
    </body>
</html>