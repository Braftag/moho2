<?php
class Date{

    
    var $days  = array("Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche");

    var $months = array("Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre");
    
    var $heure = array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');

    function getEvents($year) {
        global $dbh;
        $req = $dbh->query('SELECT * FROM events WHERE YEAR(date)='.$year);
       
        $r = array();
        while ($d = $req->fetch(PDO::FETCH_OBJ)){
            $r[strtotime($d->date)][$d->id] = $d->title;
        }
        return $r;  
    }
    
    function getEventsHour($day) {
        global $dbh;
        $req = $dbh->query('SELECT * FROM events WHERE DAY(date)='.$day);
        $r = array();
        while ($d = $req->fetch(PDO::FETCH_OBJ)){
            $r[strtotime($d->date)][$d->id] = $d->title;
        }
        return $r;  
    }  

    function getSemaine($year){
        $date = new DateTime($year.'-01-01');
        while($date ->format('Y') <= $year){

        }
    }

    function getAll($year) {
        $r = array();
        // version procedurale 
        //---------------------------------- 
        // $date = strtotime($year.'-01-01');
        // while(date('Y',$date) <= $year) { // tant que l'annes de ma date est inferieur a $year
        //     $y = date('Y',$date); // années
        //     $m = date('n',$date); // mois
        //     $d = date('j',$date); // jours
        //     $w = str_replace('0','7',date('w',$date)); // jours de la semaine 1 = lundi, 2 = mardi ...
        //     $r[$y][$m][$d] = $w;
        //     $date = strtotime(date('Y-m-d',$date)."+1 DAY");
        // }
        // ---------------------------------------
        //version objets
        $date = new DateTime($year.'-01-01');
        while($date ->format('Y') <= $year) { // tant que l'annes de ma date est inferieur a $year
            $y =  $date->format('Y');// années
            $m = $date->format('n'); // mois
            $d = $date->format('j'); // jours
            $h = $date->format('G'); // Heure
            $w = str_replace('0','7',$date->format('w')); // jours de la semaine 1 = lundi, 2 = mardi ...
            $r[$y][$m][$d] = $w;
            $date->add(new DateInterval("P1D"));
        }
        return $r;
    }
}

?>