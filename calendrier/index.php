<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="crossorigin="anonymous"></script>
    <script>

jQuery(function($) {
    $('.month').hide();
    $('.month:first').show();
    $('.months a:first').addClass('w3-red');
    $('.jour').hide();
    $('#jou').hide();
    $('.semaine').hide();
    $('.annees').hide();
    $('.w3-modal-content').draggable();

    // $('.months a').hide();
    // $(".months a:first").show();

    var current = 1;
    $('.months a').click(function(){
        var month = $(this).attr('id').replace('linkmonths','');
        if(month != current){
            $('#month'+current).slideUp();
            $('#month'+month).slideDown();
            $('.months a').removeClass('w3-red');
            $('.months a#linkmonths'+month).addClass('w3-red');
            current = month;

        }
        return false;
    });
    $('#info').click(function(){
        var select = $("#info option:selected").text();
        console.log(select);
        if(select == "Jour") {
            $('.jour').show();
            $('#jou').show();
        }
        else {
            $('.jour').hide();
            $('#jou').hide();
        }
        if(select == "Semaine") {
            $('.semaine').show();
        }
        else {
            $('.semaine').hide();
        }
        if(select == "Mois") {
            $('.period').show();
            $('#mon').show();
        }
        else {
            $('.period').hide();
            $('#mon').hide();
        }
        if(select == "Années") {
            $('.annees').show();
        }
        else {
            $('.annees').hide();
        }
    })

});
</script>
</head>
<body>

    <?php
    require ('date.php');
    require ('dbManager.php');

    $date = isset($_GET['date']) ? $_GET['date'] : null;
    $event = isset($_GET['events']) ? $_GET['events'] : null;
    $heure = isset($_GET['hour']) ? $_GET['hour'] : null;
    $text = isset($_GET['text']) ? $_GET['text'] : null;


    $year = date('Y');
    $day = date('j');


    $monthss = date('m');


    if ($heure < 10) {

        $heure = str_replace($heure, "0" + $heure, $heure);
    }

    if ($heure == null){
        $heure == '00';
    }

    $send = $date. " ". $heure. ":00:00";


    if($date && $event != null) {
        $verify = $dbh->query("INSERT INTO `events` (`title`, `date`) VALUES ('$event','$send')");
    }

    $date = new Date();
    $mois = $date->months[2];
    $dates = $date->getAll($year);
    $events = $date->getEvents($year);
    $eventHour = $date->getEventsHour($day);
    ?>
            <div class="w3-section w3-display-container w3-display-topright" id ="info">
                <select class="w3-select" name="option" required style="width: 100%">
                <option value="Jour" selected>Jour</option>
                <!-- <option value="Semaine">Semaine</option> -->
                <option value="Mois">Mois</option>
                <!-- <option value="Annees">Années</option> -->
                </select>
            </div>
    <div class='period'>
        <div class="year"><?php echo $date->months[date("m") -1]. " ". $year ?>
        </div>
        <div class="months">
            <ul>
                <?php
                foreach($date->months as $id=>$m){
                    // echo "<a href='' id='prev'><</a>";
                    echo "<li><a href='' id='linkmonths". intval($id+1) ."'>".$m."</a></li>";
                    // echo "<a href='' id='next'>></a>";
                }
                ?>
            </ul>
        </div>
        <?php $dates = current($dates);?>
        <?php
        foreach ($dates as $m=>$days) {
            echo "<div class='month' id='month".$m ."'>";
            echo "<table id'container'>";
            echo "<thead>";
            echo "<tr>";
                foreach ($date->days as $d) {
                    echo "<th>";
                    echo substr($d,0,1);
                    echo "</th>";
                }
            echo "</tr>";
            echo "</thead>";
            echo "<tbody id='main'>";
            echo "<tr>";
                $end = end($days);
                foreach ($days as $d=>$w) {
                    $time = strtotime("$year-$m-$d 00:00:00");
                    if ($d == 1) {
                        if (intval($w-1) == 0){
                            echo "<td colspan ='". intval($w-1) . "' style ='display: none'></td>";
                        }
                        else {
                            echo "<td colspan ='". intval($w-1) . "' class='non'></td>";
                        }
                    }
                    echo "<td>";
                    //echo "<a href=''>";
                    echo "<div class='relative'>";
                    echo "<button id='modal' onclick='document.getElementById(`id01`).style.display=`block`; jour(".$d.",".$m.")'>";
                    echo "<div class='day'>". $d;
                    echo "<ul class='events'>";
                        if(isset($events[$time])){
                            foreach($events[$time] as $e) {
                                echo "<li>".$e."</li>";
                            }
                        }
                    echo "</ul>";
                    echo "</div>";
                    echo "</td>";
                    //echo "</a>";
                    echo "</button>";
                    if($w == 7) {
                        echo "</tr><tr>";
                    }
                }
                // if(Send != 7) {
                //     echo "<td colspan ='". intval(7-$end) . "'></td>";
                // }
            echo "</tr>";
            echo "</tbody>";
            echo "</table>";
            echo "</div>";

        }


        ?>
    </div>


<div class="jour">
                <?php
                    echo'<div class="left year">'.$date->months[date("m") - 1]. " ". $year. " ";
                    echo "</br>";
                    echo "</div>";
                    echo"<table>";
                    echo"<thead>";
                    echo "<th>";
                    echo "<div class='center' id='day'>".$day."</div>";
                    echo "</th>";
                    echo"</thead>";
                    echo"<tbody>";
                ?>
            <tr>
                <?php foreach($date->heure as $h){
                    $timee = strtotime("$h".":00:00");
                    echo "<th class ='wesh'>";
                    echo "<div class='hour'>";
                    echo "<div class='left' id='hour". $h ."'>";
                    echo "<button id='modal' onclick='document.getElementById(`id02`).style.display=`block`'>";
                    echo $h . "H";
                    echo "<ul class='events'>";
                    if(isset($eventHour[$timee])){
                        foreach($eventHour[$timee] as $e) {
                            echo "<li>".$e."</li>";
                        }
                    }
                    echo "</ul>";
                    echo "</button>";
                    echo "</div>";
                    echo "</th>";
                } ?>
            </tr>
        </tbody>
    </table>
</div>

<div id="giroud"> </div>



  <div id="id01" class="w3-modal">
    <div class="w3-modal-content">
      <header class="w3-container w3-teal">
        <span onclick="document.getElementById('id01').style.display='none'"
        class="w3-button w3-display-topright">&times;</span>
        <h2 class="w3-center">Evenement</h2>
      </header>
      <form name="myForm" class="w3-margin-right-32"  id="mon">
      <div class="w3-section">
        <label>Date de l'evenement</label>
        <input class="w3-input w3-border w3-animate-input w3-center" type="date" required name="date" id="date" style="width: 50%">
      </div>
      <label>Heure de l'evenement</label>
            <input class="w3-input w3-border w3-animate-input w3-center" type="number" value="<?php $d ?>" required name="hour" id="dates" style="width: 50%">
      <div class="w3-section">
        <label>Evenement</label>
        <input class="w3-input w3-border w3-animate-input" type="text"  id="event" required name="events" style="width: 50%">
      </div>
      <button type="button" class="w3-button w3-block w3-dark-grey" id="submit" onclick="insert()"  style="width: 50%">Envoyer</button>
    </form>
    </div>
    </div>
  </div>
</div>


<div id="id02" class="w3-modal" onclick="fermer('#id02')">
    <div class="w3-modal-content">
      <header class="w3-container w3-teal">
        <span onclick="document.getElementById('id02').style.display='none'"
        class="w3-button w3-display-topright">&times;</span>
        <h2 class="w3-center">Evenement</h2>
      </header>
      <form name="myForm" class="w3-margin-right-32" action="index.php" method="post" id="jou">
      <div class="w3-section">
        <label>Date de l'evenement</label>
        <input class="w3-input w3-border w3-animate-input w3-center" type="date" required name="date" id="datee" style="width: 50%">
      </div>
        <div class="w3-section">
            <label>Heure de l'evenement</label>
            <input class="w3-input w3-border w3-animate-input w3-center" type="number"  required name="hour" id="datess" style="width: 50%">
        </div>
        <div class="w3-section">
            <label>Evenement</label>
            <input class="w3-input w3-border w3-animate-input" type="text"  id="events" required name="events" style="width: 50%">
        </div>
        <button type="button" onclick="insert2()" class="w3-button w3-block w3-dark-grey" id="submit" style="width: 50%">Envoyer</button>
        </form>
        </div>
    </div>
    </div>
  </div>
</div>

<div id="id03" class="w3-modal">
    <div class="w3-modal-content" style='background : none;'>
        <div id="r"></div>
    </div>
</div>


<!--<button class="w3-button" onclick="essais()">JSON ! </button> -->

        <?php $datess = strtotime("2019-01-01");
            $s = date("W");
        ?>
    <script>

        function insert() {

        var dates = $("#dates").val();
        console.log(dates)

        var event = $("#event").val();
        console.log(event)
        var date = $("#date").val();
        console.log(date)

        if (dates == "") {
            alert("l'heure doit etre valide");
            return false;
        }
        if (event == ""){
          alert("l'evenement doit etre valide")
          return false;
        }
        if (date == ""){
          alert("le jour doit etre valide")
          return false;
        }
        else {
        console.log(window.XMLHttpRequest)
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        $('#id01').hide()
        //$('#id03').show()
        //$('#r').html('<img src="load.gif" width="20%">');
        xmlhttp.open("GET","insert.php?date="+date+"&events="+event+"&hour="+dates,true);
        xmlhttp.send();
        console.log($('body').load('index.php'));
        xmlhttp.send();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //window.location.reload();
            }
        }
        }}


    </script>

<script>
        function insert2() {
            var dates = $("#datess").val();
        console.log(dates)

        var event = $("#events").val();
        console.log(event)
        var date = $("#datee").val();
        console.log(date);
        console.log(window.XMLHttpRequest)
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        $('#id02').hide()
        $('#id03').show()
        $('#r').html('<img src="load.gif" width="20%">');
        xmlhttp.open("GET","index.php?date="+date+"&events="+event+"&hour="+dates,true);
        xmlhttp.send();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //window.location.reload();
            }
        }
        }

</script>

<script>

function jour(d,m) {
  console.log(d);
  console.log(m);
  if (d < 10 & m < 10){
  var donne = "2019-0"+m+"-"+"0"+d;
  }
  if (d < 10 & m >= 10) {
      var donne = "2019-"+m+"-"+"0"+d;
  }
  if (d >= 10 & m < 10) {
      var donne = "2019-0"+m+"-"+d;
  }
  if (d >= 10 & m >= 10) {
      var donne = "2019-"+m+"-"+d;
  }
  console.log(donne);
  $("#date").val(donne);

}

function obtenirParametre (sVar) {
  return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
</script>
</body>
</html>
